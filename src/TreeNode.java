
/*
 * Source:
 * https://myarch.com/treeiter/guide/
 * http://enos.itcollege.ee/~jpoial/algoritmid/puud.html
 * http://journal.stuffwithstuff.com/2011/03/19/pratt-parsers-expression-parsing-made-easy/
 * http://maltcms.sourceforge.net/developers/api/maltcms-1.0/cross/io/misc/FragmentStringParser.html
 * http://stackoverflow.com/questions/7251781/java-method-for-parsing-nested-expressions
 * https://docs.oracle.com/javase/7/docs/api/java/util/StringTokenizer.html
 * http://stackoverflow.com/questions/7021074/string-delimiter-in-string-split-method
 * Task:
 * http://enos.itcollege.ee/~jpoial/algoritmid/PuupraktikumNew.html 
*/
import java.lang.reflect.Array;
import java.util.*;
public class TreeNode implements Iterator<TreeNode> {

	private String name;
	private TreeNode firstChild;
	private TreeNode nextSibling;

	TreeNode(String n, TreeNode d, TreeNode r) {
		this.setName(n);
		this.setFirstchild(d);
		this.setNextSibling(r);
		// TODO!!! Your constructor here
	}

	public void setName(String s) {
		name = s;

	}

	public String getName() {
		return name;

	}

	public void setFirstchild(TreeNode o) {
		firstChild = o;

	}


	public void setNextSibling(TreeNode p) {
		nextSibling = p;

	}

	public TreeNode getNextSibling() {
		return nextSibling;
	}

	public boolean treeHasFirstChild() {

		return !(this.firstChild == null); // kui puul on "lapsi"

	}

	public boolean hasNext() {

		return (getNextSibling() != null); // kui puul on "vennad"

	}

	public TreeNode next() {
		return getNextSibling(); // järgmise "venna" jaoks
	}

	public static TreeNode parsePrefix(String s) {
		if (s.isEmpty()) { // testin kas on tühi
			throw new RuntimeException("Nothing, empty" + s);
		}
		
		if (s.contains("()")) { // testin kas on tühja sulgu
			throw new RuntimeException("there are empty brackets in treenode" + s);
		}
		
		// string sümboliteks
		String treeName = new String();
		StringTokenizer st = new StringTokenizer(s, "(,)", true);
		ArrayList<String> treeSymbols = new ArrayList();
		while (st.hasMoreElements()) { // kui rohkem elemente on
			treeSymbols.add(st.nextToken()); // lisan

		}
		if (treeSymbols.get(0).equals(" ")) { // kui tuleb tühik
			throw new RuntimeException("can't find treenode name");
		
		} else if (!(treeSymbols.get(0).contains("(")||treeSymbols.get(0).contains(")") || treeSymbols.get(0).contains(",")
				|| treeSymbols.get(0).contains(" ") || treeSymbols.get(0).contains("\t"))) { // kui																	// tuleb
																							// (,),tühik,koma
			treeName = treeSymbols.get(0);
		} else {
			throw new RuntimeException("Remove wrong characters from treenode name!" + treeName);
		}
		treeSymbols.remove(0); // need elemendid mida vaja ei lähe
		// testin vormistust
		int test1 = 0;
		int test2 = 0;
		for (int i = 0; i < treeSymbols.size(); i++) {
			if (treeSymbols.get(i).equals("(")) {
				test1++;
			}
			else if (treeSymbols.get(i).equals(")")) {
				test2++;
			}
		}
		// kui on vale vormistus
		if (treeSymbols.size() > 1 && test1 != test2) {
			throw new RuntimeException("COntrol brackets!" + s);
		}

		if (test1 == 0 && test2 == 0 && treeSymbols.size() > 1) {
			throw new RuntimeException("Add brackets!" + s);
		}
		// lapsed
		String temp = new String();
		for (int i = 0; i < treeSymbols.size(); i++) {
			temp += treeSymbols.get(i);
		}
		List<String> children = new ArrayList<String>();// laste list
		int j = 0;
		int h = 0;
		if (temp.length() > 0 && temp.charAt(0) == '(') {
			for (int i = 0; i < temp.length(); i++) {
				if (temp.charAt(i) == '(') {
					j++; // suurendan
				}

				else if (temp.charAt(i) == ')') {
					if (j == 0) {
						break;
					} else if (j == 1) {
						children.add(temp.substring(h + 1, i));
						h = i; // on võrdsed
					} else {
						j--;
					}
				} else if (temp.charAt(i) == ',' && j == 1) {
					children.add(temp.substring(h + 1, i));
					h = i; // sama

				}
			}
		} else {
			return new TreeNode(treeName, null, null); // uue tagastamine
		}
		TreeNode child = null;
		for (int g = children.size() - 1; g >= 0; g--) {
			TreeNode value = TreeNode.parsePrefix(children.get(g));
			value.nextSibling = child; // uus "vend" on laps
			child = value;
		}
		return new TreeNode(treeName, child, null);
	}


	public String rightParentheticRepresentation() {
		StringBuffer b = new StringBuffer();
		b.append(name);
		if (this.hasNext()) {
			b.append(",");
			b.append(this.nextSibling.rightParentheticRepresentation());
		}
		if (this.treeHasFirstChild()) {
			b.insert(0, ")");
			b.insert(0, this.firstChild.rightParentheticRepresentation());
			b.insert(0, "(");
		}
		// TODO!!! create the result in buffer b
		return b.toString();
	}

	public static void main(String[] param) {
		String s = "A(B(C,D(E)),F)";
		TreeNode t = TreeNode.parsePrefix(s);
		String v = t.rightParentheticRepresentation();
		System.out.println(s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
	}
}
